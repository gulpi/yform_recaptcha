<?php

/**
 * yform recaptcha extension.
 *
 * @author martin.adler[at]gulpi[dot]de Gulpi (=Martin Adler)
 * @author <a href="http://www.gulpi.de">www.gulpi.de</a>
 */

class rex_yform_value_recaptcha extends rex_yform_value_abstract
{
    public function enterObject()
    {
        rex_login::startSession();

        $this->captcha_ini = parse_ini_string($this->captacha_ini(), true);
        extract($this->captcha_ini);

        $captchaRequest = rex_request('recaptcha', 'string');

        if ($captchaRequest == 'show') {
            exit;
        }
        
        $recaptcha_result = false;
        
        if ($this->params['send'] == 1) {
			$response = $_POST["g-recaptcha-response"];
			if ($response != '') {
				$url = 'https://www.google.com/recaptcha/api/siteverify';
				$data = array(
					'secret' => $this->getElement(2),
					'response' => $_POST["g-recaptcha-response"]
				);
				$options = array(
					'http' => array (
						'method' => 'POST',
						'content' => http_build_query($data)
					)
				);
				$context  = stream_context_create($options);
				$verify = file_get_contents($url, false, $context);
				$captcha_success=json_decode($verify);
				$recaptcha_result = $captcha_success;
			}
		}
		
        if ($this->params['send'] == 1 && $recaptcha_result) {
            $_SESSION['captcha'] = '';
        } elseif ($this->params['send'] == 1) {
            // Error. Fehlermeldung ausgeben
            $this->params['warning'][$this->getId()] = $this->params['error_class'];
            $this->params['warning_messages'][$this->getId()] = $this->getElement(3);
        }

        if (!$this->needsOutput()) {
            return;
        }

        if ($this->getElement(4) != '') {
            $link = $this->getElement(4);
            if (preg_match("/\?/", $link)) {
                if (mb_substr($link, -1) != '&') {
                    $link .= '&';
                }
            } else {
                $link .= '?';
            }

            $link .= 'recaptcha=show&' . time();
        } else {
            $link = rex_getUrl($this->params['article_id'], $this->params['clang'], ['recaptcha' => 'show'], '&') . '&' . time() . str_replace(' ', '', microtime());
        }

        $this->params['form_output'][$this->getId()] = $this->parse('value.recaptcha.tpl.php', ['link' => $link]);
    }

    public function getDescription()
    {
        return 'recaptcha|site-key|site-secret|Fehlertext|[link]';
    }

    public function captacha_ini()
    {
        return '';
    }
}
