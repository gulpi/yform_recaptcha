<?php

$class = 'form-captcha';
$class .= $this->getElement('required') ? 'form-is-required ' : '';
$notice = $this->getElement('notice') != '' ? '<p class="help-block">' . $this->getElement('notice') . '</p>' : '';

$class_group = trim('form-group ' . $class . $this->getWarningClass());
$class_control = trim('form-control');

$class_label = '';
$field_before = '';
$field_after = '';

if (trim($this->getElement('grid')) != '') {
    $grid = explode(',', trim($this->getElement('grid')));

    if (isset($grid[0]) && $grid[0] != '') {
        $class_label .= ' ' . trim($grid[0]);
    }

    if (isset($grid[1]) && $grid[1] != '') {
        $field_before = '<div class="' . trim($grid[1]) . '">';
        $field_after = '</div>';
    }
}
?>
<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="<?php echo $class_group ?>">
    <label class="control-label<?php echo $class_label; ?>" for="<g-recaptcha">&nbsp;</label>
    <?php echo $field_before; ?>
    <div class="input-group">
		<div id="g-recaptcha" class="g-recaptcha" data-sitekey="<?php echo $this->getElement(1) ?>"></div>
    </div>
    <?php echo $notice ?>
    <?php echo $field_after; ?>
</div>
