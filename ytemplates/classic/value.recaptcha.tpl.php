<script src='https://www.google.com/recaptcha/api.js'></script>
<p class="formcaptcha" id="<?php echo $this->getHTMLId() ?>">
    <label class="active captcha <?php echo $this->getWarningClass() ?>" for="<?php echo $this->getFieldId() ?>">&nbsp;</label>
    <span class="as-label <?php echo $this->getWarningClass() ?>"><div class="input-group"><div id="g-recaptcha" class="g-recaptcha" data-sitekey="<?php echo $this->getElement(1) ?>"></div></div></span>
</p>
