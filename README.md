﻿# yform Recaptcha - form implementation with Google ReCAPTCHA #

## Motivation ##

The REDAXO 5 internal captcha protected forms were not working any longer, a lot of spam and abuse sent thru the contact form, which caused too much extra work, so we needed a fast solution.

## Project ##

We copied a the actual yform module and changed it to Google Recaptcha with API code defined in settings. The rest of code remained unchanged and is (c) by jan.kristinus[at]redaxo[dot]org

## Status ##

Production since 2019

## Known issues ##

Data protection as it's a Google service, had to be included in consent form and data protection page. Anyway, using external services like Google can be seen critical, you could argue that even for displaying Google Recaptcha an opt in is necessary.

## Licence ##

Any action allowed no attribution necessary

## Author ##

Martin Adler, martin.adler[at]instruct[dot].eu for any questions contact me via bitbucket
